package com.kosign.demoviewpager2;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemViewHolder>{
    private ArrayList<Model_Items> mMainList;
    public ItemsAdapter(ArrayList<Model_Items> list){
        this.mMainList = list;
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.items_slider,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.onBindItem(mMainList.get(position));
    }

    @Override
    public int getItemCount() {
        return mMainList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder{
        private ImageView image;
        private TextView title;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            title = itemView.findViewById(R.id.tv_title);
        }

        public void onBindItem(Model_Items items){
            image.setImageResource(items.getImage());
            title.setText(items.getTitle());
        }
    }
}
