package com.kosign.demoviewpager2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.kosign.demoviewpager2.adapter.ExpandableListAdapter;
import com.kosign.demoviewpager2.helps.FlexibleToolBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeActivity extends AppCompatActivity{
    private ExpandableListAdapter mAdapter;
    private List<String> mListDataHeader;
    private HashMap<String,List<String>> mListDataChild;
    private ExpandableListView expandableListView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FlexibleToolBar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initView();
        initToolbar();
        onPrepareListData();
        mAdapter = new ExpandableListAdapter(this,mListDataHeader,mListDataChild);
        expandableListView.setAdapter(mAdapter);
        onAlwaysExpandGroupView();

    }

    public void initView(){
        mToolbar            = findViewById(R.id.toolbar);
        expandableListView  = findViewById(R.id.lvExp);
        swipeRefreshLayout  = findViewById(R.id.pull_refresh);
        /**
         * call event
         * */
        onClickedChildItems(expandableListView);
        onClickedGroupExpanded(expandableListView);
        onClickedGroupCollapsed(expandableListView);
        //onLongCollapsed(expandableListView);
        onPull_Refresh(swipeRefreshLayout);
    }

    public void initToolbar(){
        mToolbar.setToolBarTitle(getResources().getString(R.string.toolbar_title),R.color.color_747474);
        mToolbar.setToolBarTitleMaxLength(getResources().getString(R.string.toolbar_title).length());
        mToolbar.setBackgroundColor(getResources().getColor(android.R.color.white));
        mToolbar.setToolBarButton(FlexibleToolBar.GB_LEFT, R.drawable.ic_navigate_before_black, 0, 0);
        mToolbar.setToolBarButton(FlexibleToolBar.GB_RIGHT, R.drawable.ic_more, 0, 0);

        mToolbar.setOnToolBarClickListener(new FlexibleToolBar.BizToolBarListener() {
            @Override
            public void onToolBarClicked(int groupButton, View view) {
                switch (groupButton){
                    case FlexibleToolBar.GB_LEFT:
                        finish();
                        break;
                    case FlexibleToolBar.GB_RIGHT:
                        Toast.makeText(HomeActivity.this, "Add more", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    public void onPrepareListData(){
        mListDataHeader = new ArrayList<>();
        mListDataChild  = new HashMap<>();

        // Adding header data
        mListDataHeader.add("Top 250");
        mListDataHeader.add("Now Showing");
        mListDataHeader.add("Coming Soon..");
        mListDataHeader.add("About US");

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add("The Shawshank Redemption");
        top250.add("The Godfather");
        top250.add("The Godfather: Part II");
        top250.add("Pulp Fiction");
        top250.add("The Good, the Bad and the Ugly");
        top250.add("The Dark Knight");
        top250.add("12 Angry Men");

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add("The Conjuring");
        nowShowing.add("Despicable Me 2");
        nowShowing.add("Turbo");
        nowShowing.add("Grown Ups 2");
        nowShowing.add("Red 2");
        nowShowing.add("The Wolverine");

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add("2 Guns");
        comingSoon.add("The Smurfs 2");
        comingSoon.add("The Spectacular Now");
        comingSoon.add("The Canyons");
        comingSoon.add("Europa Report");

        List<String> about = new ArrayList<>();
        about.add("Sim");
        mListDataChild.put(mListDataHeader.get(0), top250); // Header, Child data
        mListDataChild.put(mListDataHeader.get(1), nowShowing);
        mListDataChild.put(mListDataHeader.get(2), comingSoon);
        mListDataChild.put(mListDataHeader.get(3),about);
    }

    // Listview on child click listener
    public void onClickedChildItems(ExpandableListView view){
        view.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Toast.makeText(
                        getApplicationContext(),mListDataHeader.get(groupPosition) +groupPosition+ " : " + mListDataChild.get(mListDataHeader.get(groupPosition)).get(childPosition)+childPosition+"ID::"+id, Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
    }

    // Listview Group expanded listener
    public void onClickedGroupExpanded(ExpandableListView view){
        view.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        mListDataHeader.get(groupPosition) + " Expanded", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClickedGroupCollapsed(ExpandableListView view){
        // Listview Group collasped listener
        view.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        mListDataHeader.get(groupPosition) + " Collapsed", Toast.LENGTH_SHORT).show();

            }
        });
    }

    /**
     * collapsed when we clicked other  group  expands
     * */
    public void onLongCollapsed(final ExpandableListView view){
        view.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                for (int i = 0; i <mListDataHeader.size(); i++){
                    if (i != groupPosition) view.collapseGroup(i);
                }
            }
        });
    }

    /**
     * always expands on groupView
     * */
    public void onAlwaysExpandGroupView(){
        for (int i = 0; i < mAdapter.getGroupCount(); i++){
            expandableListView.expandGroup(i);
        }
    }

    /**
     * On Pull-Refresh
     * */
    public void onPull_Refresh(final SwipeRefreshLayout swipeRefreshLayout){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code here
                swipeRefreshLayout.setColorSchemeResources(R.color.color_0c419a);
                Toast.makeText(getApplicationContext(), "Works!", Toast.LENGTH_LONG).show();
                // To keep animation for 3 seconds
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        // Stop animation (This will be after 3 seconds)
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, 3000); // Delay in millis
            }
        });
    }

}
