package com.kosign.demoviewpager2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    public ItemsAdapter mAdapter;
    private ViewPager2 viewPager2;
    private LinearLayout layout;
    private MaterialButton mButton;
    private int mSlideCount = 3;
    private Timer mTimer;

    private ArrayList<Model_Items> mMainLists = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        initItemsData();

        setIndicatorSlide();

        setCurrentIndicator(0);

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setCurrentIndicator(position);
            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager2.getCurrentItem()+1 < mAdapter.getItemCount()){
                    viewPager2.setCurrentItem(viewPager2.getCurrentItem()+1);
                }else {
                    //intent to new activity
                    Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                    startActivity(intent);
                }

            }
        });
    }
    //register view
    public  void initView(){
        viewPager2 = findViewById(R.id.viewpager2);
        layout     = findViewById(R.id.dot);
        mButton    = findViewById(R.id.btnAction);
    }
    public void initItemsData(){

        Model_Items item1 = new Model_Items();
        item1.setImage(R.drawable.sim);
        item1.setTitle("Hello World");

        Model_Items item2 = new Model_Items();
        item2.setImage(R.drawable.sim);
        item2.setTitle("My name is RATE");

        Model_Items item3 = new Model_Items();
        item3.setImage(R.drawable.sim);
        item3.setTitle("KA KA");

        Model_Items item4 = new Model_Items();
        item4.setImage(R.drawable.sim);
        item4.setTitle("Ronaldo");

        //add all object item to Main list
        mMainLists.add(item1);
        mMainLists.add(item2);
        mMainLists.add(item3);
        mMainLists.add(item4);

        //add to Adapter
        mAdapter = new ItemsAdapter(mMainLists);
        viewPager2.setAdapter(mAdapter);
    }

    //show all indicators
    public void setIndicatorSlide(){
        ImageView[] indicators = new ImageView[mAdapter.getItemCount()];
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(8,0,8,0);
        for (int i = 0; i<indicators.length; i++){
            indicators[i] = new ImageView(getApplicationContext());
            indicators[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.active_bg));
            indicators[i].setLayoutParams(params);
            //add indicator to layout
            layout.addView(indicators[i]);
        }
    }
    @SuppressLint("SetTextI18n")
    public void setCurrentIndicator(int index){
        int child = layout.getChildCount();
        for (int i = 0; i<child; i++){
            //declare view for store indicator at position
            ImageView imageStore = (ImageView) layout.getChildAt(i);
            if (i == index){
                imageStore.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.inactive_bg));
            }else {
                imageStore.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.active_bg));
            }
        }

        if (index == mAdapter.getItemCount()-1){
            mButton.setText("Start");
        }else {
            mButton.setText("Next");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //startSliderTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //stopSliderTimer();
    }

    /**
     * start timer
     */
    public void startSliderTimer() {
        viewPager2.setCurrentItem(mSlideCount - 1); //reset view pager index zero

        if (mSlideCount > 0) {
            if (mTimer != null) {
                return;
            }
            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(new SliderTimer(), 0, 4000);
        }
    }

    /**
     * control slide timer
     */
    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager2.getCurrentItem() < mSlideCount -1) {
                        viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
                    } else {
                        viewPager2.setCurrentItem(0);
                    }
                }
            });
        }
    }

    /**
     * stop timer
     */
    public void stopSliderTimer() {
        viewPager2.setCurrentItem(0); //reset view pager index zero
        if (mTimer != null) {
            if (mSlideCount > 0) {
                mTimer.cancel();
                mTimer = null;
            }
        }
    }
}
