package com.kosign.demoviewpager2;

public class Model_Items {
    private int image;
    private String title;
    public Model_Items(){}
    public Model_Items(int image, String title) {
        this.image = image;
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
